

package co.gladepay.android.ui;

import android.app.Activity;



import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import co.gladepay.android.R;
import co.gladepay.android.design.widget.PinPadView;
import co.gladepay.android.ui.OtpSingleton;

public class OtpActivity extends AppCompatActivity {

    final OtpSingleton si = OtpSingleton.getInstance();
    private PinPadView pinpadView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.co_gladepay_android____activity_otp);
        setTitle("ENTER OTP");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        pinpadView = findViewById(R.id.pinpadView);
        setup();
    }

    protected void setup() {


        pinpadView.setPromptText(si.getOtpMessage());
        pinpadView.setVibrateOnIncompleteSubmit(false);
        pinpadView.setAutoSubmit(false);

        pinpadView.setOnPinChangedListener(new PinPadView.OnPinChangedListener() {
            @Override
            public void onPinChanged(String oldPin, String newPin) {
                // We had set length to 10 while creating,
                // but in case some otp is longer,
                // we will keep increasing pin length
                if(newPin.length() >= pinpadView.getPinLength()){
                    pinpadView.setPinLength(pinpadView.getPinLength()+1);
                }
            }
        });

        pinpadView.setOnSubmitListener(new PinPadView.OnSubmitListener() {
            // Always submit (we never expect this to complete since
            // we keep increasing the pinLength during pinChanged event)
            // we still handle onComplete nonetheless
            @Override
            public void onCompleted(String otp) {
                handleSubmit(otp);
            }

            @Override
            public void onIncompleteSubmit(String otp) {
                handleSubmit(otp);
            }
        });
    }

    public void onDestroy() {
        super.onDestroy();
        handleSubmit("");
    }

    public void handleSubmit(String otp){
        synchronized (si) {
            si.setOtp(otp);
            si.notify();
        }
        finish();
    }
}
