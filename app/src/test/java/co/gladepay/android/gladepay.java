package co.gladepay.android;

import android.app.Activity;

import co.gladepay.android.exceptions.AuthenticationException;
import co.gladepay.android.exceptions.gladepaySdkNotInitializedException;
import co.gladepay.android.model.Charge;
import co.gladepay.android.model.gladepayModel;
import co.gladepay.android.utils.Utils;


public class gladepay extends gladepayModel {

    private String publicKey;

    /**
     * Constructor.
     */
    protected gladepay() throws gladepaySdkNotInitializedException {
        //validate sdk initialized
        Utils.Validate.validateSdkInitialized();
    }

    protected gladepay(String publicKey) throws AuthenticationException {
        setPublicKey(publicKey);
    }

    /**
     * Sets the public key
     *
     * @param publicKey - App Developer's public key
     */
    private void setPublicKey(String publicKey) throws AuthenticationException {
        //validate the public key
        validatePublicKey(publicKey);
        this.publicKey = publicKey;
    }

    private void validatePublicKey(String publicKey) throws AuthenticationException {
        //check for null value, and length and startswith pk_
        if (publicKey == null || publicKey.length() < 1 || !publicKey.startsWith("pk_")) {
            throw new AuthenticationException("Invalid public key. To create a token, " +
                    "you must use a valid public key.\nEnsure that you have set a public key." +
                    "\nCheck http://paystack.co for more");
        }

    }

    void chargeCard(Activity activity, Charge charge, TransactionCallback transactionCallback) {
        chargeCard(activity, charge, publicKey, transactionCallback);
    }


    private void chargeCard(Activity activity, Charge charge, String publicKey, TransactionCallback transactionCallback) {
        //check for the needed data, if absent, send an exception through the tokenCallback;
        try {
            //validate public key
            validatePublicKey(publicKey);

            TransactionManager transactionManager = new TransactionManager(activity, charge, transactionCallback);

            transactionManager.chargeCard();

        } catch (Exception ae) {
            assert transactionCallback != null;
            transactionCallback.onError(ae, null);
        }
    }

    private interface BaseCallback {
    }

    public interface TransactionCallback extends BaseCallback {
        void onSuccess(Transaction transaction);
        void beforeValidate(Transaction transaction);

        void onError(Throwable error, Transaction transaction);
    }

}
