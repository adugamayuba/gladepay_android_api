package co.gladepay.android.api.request;

import android.provider.Settings;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

import co.gladepay.android.gladepaySdk;

/**
 * A base for all request bodies
 */
abstract class BaseRequestBody {
    static final String FIELD_DEVICE = "device";
    @SerializedName(FIELD_DEVICE)
    String device;

    public abstract HashMap<String, String> getParamsHashMap();

    void setDeviceId() {
        this.device = "androidsdk_" + Settings.Secure.getString(gladepaySdk.applicationContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

}
