package co.gladepay.android.exceptions;

import java.io.Serializable;

/**
 * Base class for exceptions
 *
 
 */
public class gladepayException extends RuntimeException implements Serializable {

    public gladepayException(String message) {
        super(message, null);
    }

    public gladepayException(String message, Throwable e) {
        super(message, e);
    }
}
