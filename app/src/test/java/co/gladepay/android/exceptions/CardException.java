package co.gladepay.android.exceptions;

/**
 * 
 */
public class CardException extends gladepayException {

    public CardException(String message) {
        super(message);
    }

    public CardException(String message, Throwable e) {
        super(message, e);
    }
}
