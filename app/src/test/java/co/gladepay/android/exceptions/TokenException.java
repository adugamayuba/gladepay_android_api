package co.gladepay.android.exceptions;

/**
  
 */
public class TokenException extends gladepayException {
    public TokenException(String message) {
        super(message);
    }
}
