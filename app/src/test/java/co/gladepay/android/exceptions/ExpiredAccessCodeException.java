package co.gladepay.android.exceptions;

/**
 

 */
public class ExpiredAccessCodeException extends gladepayException {
    public ExpiredAccessCodeException(String message) {
        super(message);
    }
}
