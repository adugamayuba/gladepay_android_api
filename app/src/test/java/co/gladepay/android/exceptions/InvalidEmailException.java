package co.gladepay.android.exceptions;

/**
 * Created by i on 24/08/2016.
 */
public class InvalidEmailException extends gladepayException {

    private String email;

    public InvalidEmailException(String email) {
        super(email + " is not a valid email");
        this.setEmail(email);
    }

    public String getEmail() {
        return email;
    }

    public InvalidEmailException setEmail(String email) {
        this.email = email;
        return this;
    }

}
