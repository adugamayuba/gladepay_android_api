package co.gladepay.android.exceptions;

/**
 
 */
public class ProcessingException extends ChargeException {
    public ProcessingException() {
        super("A transaction is currently processing, please wait till it concludes before attempting a new charge.");
    }
}
