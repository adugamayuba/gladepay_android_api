package co.gladepay.android.exceptions;

/**

 */
public class gladepaySdkNotInitializedException extends PaystackException {
    public gladepaySdkNotInitializedException(String message) {
        super(message);
    }
}
