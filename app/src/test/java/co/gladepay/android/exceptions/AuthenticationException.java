package co.gladepay.android.exceptions;

/**

 */
public class AuthenticationException extends gladepayException {
    public AuthenticationException(String message) {
        super(message);
    }

    public AuthenticationException(String message, Throwable e) {
        super(message, e);
    }
}
