package co.gladepay.android.exceptions;


public class ChargeException extends gladepayException {
    public ChargeException(String message) {
        super(message);
    }
}
