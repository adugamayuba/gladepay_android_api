package co.gladepay.android.exceptions;

/**
 
 */
public class ValidateException extends gladepayException {
    public ValidateException(String message) {
        super(message);
    }
}
