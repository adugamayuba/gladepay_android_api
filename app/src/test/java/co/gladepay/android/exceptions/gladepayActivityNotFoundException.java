package co.gladepay.android.exceptions;

/**

 */
public class gladepayActivityNotFoundException extends gladepayException {
    public gladepayActivityNotFoundException(String message) {
        super(message);
    }

    public gladepayActivityNotFoundException(String message, Throwable e) {
        super(message, e);
    }
}
